<?php
	$nombre = $_POST['nombre'];
	$correo = $_POST['correo'];
	$tamano = $_POST['tamanoempresa'];
	$mensaje = $_POST['mensaje'];
	$url = $_POST['url'];
	ob_start();
	$temario = $_POST['temariopdf'];
	if(isset($_POST['submit']) && $nombre != "" && $correo != '' && $tamano != '' && $mensaje != '')
	{
		require 'phpmailer/PHPMailerAutoload.php';
		$mail = new PHPMailer;
		$mail->setFrom("contacto@issconsultores.com.mx","ISS Consultores, descarga de temario");
		$mail->addReplyTo($correo,$nombre);
		$mail->addAddress('web@merkategia.com');
		$mail->Subject = 'ISS Consultores, descarga de temario';
		$mail->isHTML(true);
		$mail->CharSet = 'UTF-8';
		$mail->Body = '
			<html>
				<head>
					<title>ISS Consultores, descarga de temario</title>
				</head>
				<body>
					<table style="border: 0; border-collapse: collapse; width: 410px; margin: auto;">
						<tr>
							<td colspan="2" style="padding: 10px; text-align: center;">
								<img src="http://imagencomunicacionimpresa.net/issconsultores/wp-content/themes/issconsultores/images/iss-consultores-logo.png">
								<h1 style="color: #41414;">Nueva solicitud de cotización ISS Consultores</h1>
							</td>
						</tr>
						<tr>
							<td style="padding: 10px;">Nombre</td>
							<td style="padding: 10px;">'.$nombre.'</td>
						</tr>
						<tr>
							<td style="padding: 10px;">E-mail</td>
							<td style="padding: 10px;">'.$correo.'</td>
						</tr>
						<tr>
							<td style="padding: 10px;">Tamaño de empresa</td>
							<td style="padding: 10px;">'.$tamano.'</td>
						</tr>
						<tr>
							<td style="padding: 10px;">Mensaje</td>
							<td style="padding: 10px;">'.$mensaje.'</td>
						</tr>
					</table>
				</body>
			</html>
		';
		if(file_exists($temario))
		{
		    header('Content-Description: File Transfer');
		    header('Content-Type: application/octec-stream');
		    header('Content-Disposition: attachment; filename="'.basename($temario).'"');
		    header('Content-Transfer-Encoding: binary');
		    header('Expires: 0');
		    header('Content-Lenght: '.filesize($temario).'');
		    ob_clean();
		    flush();
		    readfile($temario);
		    exit();
		    header('Location: '.$url.'?success');   
		}
		if(!$mail->send())
		{
			echo "Mailer Error: " . $mail->ErrorInfo;
		}
		else
		{			
			header('Location: '.$url.'?success');
		}
	}
	else
	{
		header('Location: '.$url.'?error');
	}
?>